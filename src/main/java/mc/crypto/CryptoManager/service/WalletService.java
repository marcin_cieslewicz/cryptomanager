package mc.crypto.CryptoManager.service;

import mc.crypto.CryptoManager.dto.CreateWalletRequest;
import mc.crypto.CryptoManager.dto.ExchangeTokensRequest;
import mc.crypto.CryptoManager.dto.GetBalanceResponse;
import mc.crypto.CryptoManager.model.CurrencyType;
import mc.crypto.CryptoManager.model.ExchangeRate;
import mc.crypto.CryptoManager.model.User;
import mc.crypto.CryptoManager.model.Wallet;
import mc.crypto.CryptoManager.repository.CurrencyTypeRepository;
import mc.crypto.CryptoManager.repository.ExchangeRateRepository;
import mc.crypto.CryptoManager.repository.UserRepository;
import mc.crypto.CryptoManager.repository.WalletRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class WalletService {

    private final CurrencyTypeRepository currencyTypeRepository;

    private final UserRepository userRepository;

    private final WalletRepository walletRepository;

    private final ExchangeRateRepository exchangeRateRepository;

    public WalletService(CurrencyTypeRepository currencyTypeRepository, UserRepository userRepository, WalletRepository walletRepository,  ExchangeRateRepository exchangeRateRepository) {
        this.currencyTypeRepository = currencyTypeRepository;
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
        this.exchangeRateRepository = exchangeRateRepository;
    }

    public void createWallet(CreateWalletRequest createWalletRequest){
       CurrencyType currencyType = currencyTypeRepository.findById(createWalletRequest.getCurrencyTypeId())
               .orElseThrow(() -> new IllegalArgumentException("Currency Type Not Found"));
       User user = userRepository.findById(createWalletRequest.getUserId())
                .orElseThrow(() -> new IllegalArgumentException("User Not Found"));

       String generatedAddress = UUID.randomUUID().toString();
       while (walletRepository.existsByAddress(generatedAddress)){
           generatedAddress = UUID.randomUUID().toString();
       }

       Wallet wallet = new Wallet();
       wallet.setAddress(generatedAddress);
       wallet.setBalance(0.0);
       wallet.setCurrencyType(currencyType);
       wallet.setUser(user);

       walletRepository.save(wallet);
    }

    public GetBalanceResponse getWalletBalance(String email, Integer walletId){
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));

        Wallet requestedWallet = null;
        for (Wallet wallet : user.getWallets()){
            if (wallet.getId().equals(walletId)){
                requestedWallet = wallet;
                break;
            }
        }
        if (requestedWallet == null){
            throw new IllegalArgumentException("User wallet not found");
        }

        GetBalanceResponse getBalanceResponse = new GetBalanceResponse();
        getBalanceResponse.setAddress(requestedWallet.getAddress());
        getBalanceResponse.setBalance(requestedWallet.getBalance());
        getBalanceResponse.setCoinName(requestedWallet.getCurrencyType().getCoinName());
        getBalanceResponse.setTicker(requestedWallet.getCurrencyType().getTicker());

        return getBalanceResponse;
    }

    public void exchangeFunds(ExchangeTokensRequest exchangeTokensRequest, String email){
        Wallet fromWallet = walletRepository.findByAddress(exchangeTokensRequest.getBaseAddress())
                .orElseThrow(() -> new IllegalArgumentException("Address Not Found"));
        Wallet toWallet = walletRepository.findByAddress(exchangeTokensRequest.getTargetAddress())
                .orElseThrow(() -> new IllegalArgumentException("Address Not Found"));

        List<Integer> userWalletIdList = getUserWalletIdList(email);

        if ((!userWalletIdList.contains(fromWallet.getId())) || (!userWalletIdList.contains(toWallet.getId()))){
            throw new IllegalArgumentException("One of the address is not assigned to logged user");
        }

        if(exchangeTokensRequest.getValue() <= 0.0 || fromWallet.getBalance() -  exchangeTokensRequest.getValue() <0.0){
            throw new IllegalArgumentException("Incorrect value was given, either exceeds user balance or is less or equal to zero");
        }

        ExchangeRate exchangeRate = exchangeRateRepository.findByAssetOneAndAssetTwo(fromWallet.getCurrencyType(), toWallet.getCurrencyType())
                .orElseThrow(() -> new IllegalArgumentException("Only BTC/ETH/USDC pairs supported"));

        Double calculatedExchangeValue = exchangeTokensRequest.getValue() * exchangeRate.getRate();
        fromWallet.setBalance(fromWallet.getBalance() - exchangeTokensRequest.getValue());
        toWallet.setBalance(toWallet.getBalance() + calculatedExchangeValue);
    }

    private List<Integer> getUserWalletIdList(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new IllegalArgumentException("User not found"))
                .getWallets()
                .stream()
                .map(w -> w.getId())
                .collect(Collectors.toList());
    }
}
