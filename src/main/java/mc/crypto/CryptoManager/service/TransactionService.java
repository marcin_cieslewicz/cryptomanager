package mc.crypto.CryptoManager.service;

import mc.crypto.CryptoManager.dto.ExchangeTokensRequest;
import mc.crypto.CryptoManager.dto.GetAddressHistoryResponse;
import mc.crypto.CryptoManager.dto.GetBalanceResponse;
import mc.crypto.CryptoManager.dto.TransferFundsRequest;
import mc.crypto.CryptoManager.model.ExchangeRate;
import mc.crypto.CryptoManager.model.Transaction;
import mc.crypto.CryptoManager.model.User;
import mc.crypto.CryptoManager.model.Wallet;
import mc.crypto.CryptoManager.repository.ExchangeRateRepository;
import mc.crypto.CryptoManager.repository.TransactionRepository;
import mc.crypto.CryptoManager.repository.UserRepository;
import mc.crypto.CryptoManager.repository.WalletRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionService {

    private final TransactionRepository transactionRepository;

    private final UserRepository userRepository;

    private final WalletRepository walletRepository;

    public TransactionService(TransactionRepository transactionRepository, UserRepository userRepository, WalletRepository walletRepository) {
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
    }


    public void transferFunds(TransferFundsRequest transferFundsRequest, String email){
        Wallet senderWallet = walletRepository.findByAddress(transferFundsRequest.getSenderAddress())
                .orElseThrow(() -> new IllegalArgumentException("Sender address Not Found"));
        Wallet receiverWallet = walletRepository.findByAddress(transferFundsRequest.getReceiverAddress())
                .orElseThrow(() -> new IllegalArgumentException("Receiver address Not Found"));

        checkPrerequisites(email, senderWallet, receiverWallet, transferFundsRequest);

        Double value = (double)Math.round(transferFundsRequest.getValue() * 100000000d) / 100000000d;
        senderWallet.setBalance(senderWallet.getBalance() - value);
        receiverWallet.setBalance(receiverWallet.getBalance() + value);

        Transaction transaction = new Transaction();
        transaction.setSenderWallet(senderWallet);
        transaction.setReceiverWallet(receiverWallet);
        transaction.setDate(new Date());
        transaction.setValue(value);
        transactionRepository.save(transaction);
    }

    public List <GetAddressHistoryResponse> getAddressHistory(String requestedAddress){
        Wallet searchedWalletAddress = walletRepository.findByAddress(requestedAddress)
                .orElseThrow(() -> new IllegalArgumentException("Address Not Found"));

        List <GetAddressHistoryResponse> addressHistory = new ArrayList<>();

        for (Transaction outflowTransaction : searchedWalletAddress.getOutflowTransactions()) {
            GetAddressHistoryResponse getAddressHistoryResponse = new GetAddressHistoryResponse();
            getAddressHistoryResponse.setSenderAddress(searchedWalletAddress.getAddress());
            getAddressHistoryResponse.setReceiverAddress(outflowTransaction.getReceiverWallet().getAddress());
            getAddressHistoryResponse.setTransactionDate(outflowTransaction.getDate());
            getAddressHistoryResponse.setTransactionValue(outflowTransaction.getValue());
            getAddressHistoryResponse.setTicker(outflowTransaction.getReceiverWallet().getCurrencyType().getTicker());
            addressHistory.add(getAddressHistoryResponse);
        }

        for (Transaction inflowTransaction : searchedWalletAddress.getInflowTransactions()){
            GetAddressHistoryResponse getAddressHistoryResponse = new GetAddressHistoryResponse();
            getAddressHistoryResponse.setSenderAddress(inflowTransaction.getSenderWallet().getAddress());
            getAddressHistoryResponse.setReceiverAddress(searchedWalletAddress.getAddress());
            getAddressHistoryResponse.setTransactionDate(inflowTransaction.getDate());
            getAddressHistoryResponse.setTransactionValue(inflowTransaction.getValue());
            getAddressHistoryResponse.setTicker(inflowTransaction.getReceiverWallet().getCurrencyType().getTicker());
            addressHistory.add(getAddressHistoryResponse);
        }

        Collections.sort(addressHistory, (x, y) -> x.getTransactionDate().compareTo(y.getTransactionDate()));

        return addressHistory;
    }

    private List<Integer> getUserWalletIdList(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new IllegalArgumentException("User not found"))
                .getWallets()
                .stream()
                .map(w -> w.getId())
                .collect(Collectors.toList());
    }

    private void checkPrerequisites(String email, Wallet senderWallet, Wallet receiverWallet, TransferFundsRequest transferFundsRequest){ //using findByEmail first because of jwt token auth, then map user's wallet list to walletid list
        List<Integer> walletIdList = getUserWalletIdList(email);

        if (!walletIdList.contains(senderWallet.getId())){
            throw new IllegalArgumentException("Sender address does not match with logged user");
        }

        if(transferFundsRequest.getValue() <= 0.0 || senderWallet.getBalance() -  transferFundsRequest.getValue() <0.0){
            throw new IllegalArgumentException("Incorrect value was given, either exceeds user balance or is less or equal to zero");
        }

        if(!senderWallet.getCurrencyType().getId().equals(receiverWallet.getCurrencyType().getId())){
            throw new IllegalArgumentException("Currency type mismatch");
        }
    }
}
