package mc.crypto.CryptoManager.service;


import mc.crypto.CryptoManager.dto.DepositFundsRequest;
import mc.crypto.CryptoManager.model.Wallet;
import mc.crypto.CryptoManager.repository.CurrencyTypeRepository;
import mc.crypto.CryptoManager.repository.WalletRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class FundsService {

    private final CurrencyTypeRepository currencyTypeRepository;

    private final WalletRepository walletRepository;

    public FundsService(CurrencyTypeRepository currencyTypeRepository, WalletRepository walletRepository) {
        this.currencyTypeRepository = currencyTypeRepository;
        this.walletRepository = walletRepository;
    }

    public void depositFunds(DepositFundsRequest depositFundsRequest){
        Wallet wallet = walletRepository.findByAddress(depositFundsRequest.getAddress())
                .orElseThrow(() -> new IllegalArgumentException("Address Not Found"));
        if (depositFundsRequest.getDeposit()<0.0){
            throw new IllegalArgumentException("Negative deposit value");
        }

        Double deposit = (double)Math.round(depositFundsRequest.getDeposit() * 100000000d) / 100000000d;
        wallet.setBalance(wallet.getBalance() + deposit);

        walletRepository.save(wallet);
    }


}
