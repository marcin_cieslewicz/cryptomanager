package mc.crypto.CryptoManager.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import mc.crypto.CryptoManager.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CryptoUserDetails implements UserDetails {

    private final Integer id;

    private final String email;

    private final String userName;

    @JsonIgnore
    private final String password;

    public CryptoUserDetails(
            Integer id, String email, String userName, String password
    ) {
        this.id = id;
        this.email = email;
        this.userName = userName;
        this.password = password;
    }

    public static CryptoUserDetails build(User user) {
        return new CryptoUserDetails(
                user.getId(),
                user.getEmail(),
                user.getUserName(),
                user.getPassword()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getEmail() {
        return email;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public String getPassword() {
        return password;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
