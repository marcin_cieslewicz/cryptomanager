package mc.crypto.CryptoManager.security;

import mc.crypto.CryptoManager.model.User;
import mc.crypto.CryptoManager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class CryptoUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtils jwtUtils;

    // Email is used as username
    @Override
    public CryptoUserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + email));
        return CryptoUserDetails.build(user);
    }

    public String getEmailFromAuthorizationHeader(ServletRequestAttributes servletRequestAttributes) {
        String token = servletRequestAttributes.getRequest().getHeader("Authorization").split(" ")[1];
        return jwtUtils.getEmailFromJwtToken(token);
    }

}
