package mc.crypto.CryptoManager.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DepositFundsRequest {

    @NotNull
    private Double deposit;

    @NotBlank
    private String address;

    public Double getDeposit() {return deposit;}

    public void setDeposit(Double deposit) {this.deposit = deposit;}

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
