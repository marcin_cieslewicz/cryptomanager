package mc.crypto.CryptoManager.dto;

import javax.validation.constraints.NotNull;

public class ExchangeTokensRequest {
    @NotNull
    private String baseAddress;

    @NotNull
    private String targetAddress;

    @NotNull
    private Double value;

    public String getBaseAddress() {
        return baseAddress;
    }

    public void setBaseAddress(String baseAddress) {
        this.baseAddress = baseAddress;
    }

    public String getTargetAddress() {
        return targetAddress;
    }

    public void setTargetAddress(String targetAddress) {
        this.targetAddress = targetAddress;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
