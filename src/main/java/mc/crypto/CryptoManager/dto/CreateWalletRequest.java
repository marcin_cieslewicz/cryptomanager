package mc.crypto.CryptoManager.dto;

import javax.validation.constraints.NotNull;

public class CreateWalletRequest {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer currencyTypeId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCurrencyTypeId() {
        return currencyTypeId;
    }

    public void setCurrencyTypeId(Integer currencyTypeId) {
        this.currencyTypeId = currencyTypeId;
    }
}
