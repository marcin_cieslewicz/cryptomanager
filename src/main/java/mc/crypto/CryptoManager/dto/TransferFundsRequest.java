package mc.crypto.CryptoManager.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TransferFundsRequest {
    @NotBlank
    private String senderAddress;

    @NotBlank
    private String receiverAddress;

    @NotNull
    private Double value;

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
