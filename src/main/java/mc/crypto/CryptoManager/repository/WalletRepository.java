package mc.crypto.CryptoManager.repository;

import mc.crypto.CryptoManager.model.User;
import mc.crypto.CryptoManager.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WalletRepository extends JpaRepository<Wallet, Integer> {
    Boolean existsByAddress(String address);
    Optional<Wallet> findByAddress(String address);
}
