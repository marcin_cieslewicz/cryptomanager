package mc.crypto.CryptoManager.repository;

import mc.crypto.CryptoManager.model.CurrencyType;
import mc.crypto.CryptoManager.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Integer> {
    Optional<ExchangeRate> findByAssetOneAndAssetTwo(CurrencyType currencyTypeOne, CurrencyType currencyTypeTwo);
}
