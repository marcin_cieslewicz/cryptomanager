package mc.crypto.CryptoManager.repository;

import mc.crypto.CryptoManager.model.CurrencyType;
import mc.crypto.CryptoManager.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyTypeRepository extends JpaRepository<CurrencyType, Integer> {
}
