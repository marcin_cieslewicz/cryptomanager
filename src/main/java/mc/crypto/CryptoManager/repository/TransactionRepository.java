package mc.crypto.CryptoManager.repository;

import mc.crypto.CryptoManager.model.CurrencyType;
import mc.crypto.CryptoManager.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
}
