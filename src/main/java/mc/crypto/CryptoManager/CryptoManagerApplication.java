package mc.crypto.CryptoManager;

import com.sun.xml.bind.v2.schemagen.xmlschema.Appinfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableSwagger2
public class CryptoManagerApplication {

	public static void main(String[] args) {

		SpringApplication.run(CryptoManagerApplication.class, args);
	}

	@Bean
	public Docket cryptoManagerApi(){
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build().apiInfo(createApiInfo());
	}

	private ApiInfo createApiInfo() {
		return new ApiInfo("Crypto Manager",
				"Crypto Manager database",
				"1.00",
				"",
				new Contact("", "", ""),
				"",
				""
		);
	}
}
