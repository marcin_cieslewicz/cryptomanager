package mc.crypto.CryptoManager.controller;


import mc.crypto.CryptoManager.dto.CreateWalletRequest;
import mc.crypto.CryptoManager.dto.DepositFundsRequest;
import mc.crypto.CryptoManager.dto.ExchangeTokensRequest;
import mc.crypto.CryptoManager.dto.auth.Response;
import mc.crypto.CryptoManager.security.CryptoUserDetailsService;
import mc.crypto.CryptoManager.service.FundsService;
import mc.crypto.CryptoManager.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.validation.Valid;

@RestController
@RequestMapping("/wallet")
public class WalletController {

    @Autowired
    private WalletService walletService;
    @Autowired
    private FundsService fundsService;
    @Autowired
    private CryptoUserDetailsService cryptoUserDetailsService;

    @PostMapping
    public ResponseEntity<?> createWallet(@Valid @RequestBody CreateWalletRequest createWalletRequest) {

        try{
            walletService.createWallet(createWalletRequest);
            return ResponseEntity.ok(
                new Response("Wallet successfully added.")
            );
        } catch (IllegalArgumentException exception){
            return ResponseEntity
                    .badRequest()
                    .body(new Response(exception.getMessage()));
        }
    }

    @PutMapping("/deposit")
    public ResponseEntity<?> depositFunds(@Valid @RequestBody DepositFundsRequest depositFundsRequest) {
        try{
            fundsService.depositFunds(depositFundsRequest);
            return ResponseEntity.ok(
                    new Response("Funds are safu.")
            );
        } catch (IllegalArgumentException exception){
            return ResponseEntity
                    .badRequest()
                    .body(new Response(exception.getMessage()));
        }
    }


    @GetMapping("/get_balance/{id}")
    public ResponseEntity<?> getBalance(@PathVariable ("id") Integer walletId) {
        try{
            String email = cryptoUserDetailsService.getEmailFromAuthorizationHeader((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes());
            return ResponseEntity.ok(
                    walletService.getWalletBalance(email, walletId)
            );
        } catch (IllegalArgumentException exception){
            return ResponseEntity
                    .badRequest()
                    .body(new Response(exception.getMessage()));
        }
    }

    @PatchMapping("/exchange")
    public ResponseEntity<?> exchangeFunds(@Valid @RequestBody ExchangeTokensRequest exchangeTokensRequest){
        try{
            String email = cryptoUserDetailsService.getEmailFromAuthorizationHeader((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes());
            walletService.exchangeFunds(exchangeTokensRequest, email);
            return ResponseEntity.ok(
                    new Response("Exchange successful")
            );
        } catch (IllegalArgumentException exception){
            return ResponseEntity
                    .badRequest()
                    .body(new Response(exception.getMessage()));
        }
    }

}
