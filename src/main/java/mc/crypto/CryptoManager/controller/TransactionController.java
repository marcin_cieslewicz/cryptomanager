package mc.crypto.CryptoManager.controller;

import mc.crypto.CryptoManager.dto.GetAddressHistoryResponse;
import mc.crypto.CryptoManager.dto.TransferFundsRequest;
import mc.crypto.CryptoManager.dto.auth.Response;
import mc.crypto.CryptoManager.security.CryptoUserDetailsService;
import mc.crypto.CryptoManager.service.FundsService;
import mc.crypto.CryptoManager.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.validation.Valid;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private FundsService fundsService;

    @Autowired

    private TransactionService transactionService;

    @Autowired
    private CryptoUserDetailsService cryptoUserDetailsService;

    @PostMapping("/transfer")
    public ResponseEntity<?> transferFunds(@Valid @RequestBody TransferFundsRequest transferFundsRequest){
        try{
            String email = cryptoUserDetailsService.getEmailFromAuthorizationHeader((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes());
            transactionService.transferFunds(transferFundsRequest, email);
            return ResponseEntity.ok(
                    new Response("Transfer successful")
            );
        } catch (IllegalArgumentException exception){
            return ResponseEntity
                    .badRequest()
                    .body(new Response(exception.getMessage()));
        }
    }

    @GetMapping("/history/{address}")
    public ResponseEntity<?> getAddressHistory(@PathVariable ("address") String requestedAddress){
        try{
            return ResponseEntity.ok(
                    transactionService.getAddressHistory(requestedAddress)
            );
        } catch (IllegalArgumentException exception){
            return ResponseEntity
                    .badRequest()
                    .body(new Response(exception.getMessage()));
        }
    }

}
