package mc.crypto.CryptoManager.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "currency_types")
public class CurrencyType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String coinName;

    private String ticker;

    @OneToMany (mappedBy = "currencyType")
    private List<Wallet> wallets;

    @OneToMany (mappedBy = "assetOne")
    private List<ExchangeRate> assetOneRates;

    @OneToMany (mappedBy = "assetTwo")
    private List<ExchangeRate> assetTwoRates;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }

    public List<ExchangeRate> getAssetOneRates() {
        return assetOneRates;
    }

    public void setAssetOneRates(List<ExchangeRate> assetOneRates) {
        this.assetOneRates = assetOneRates;
    }

    public List<ExchangeRate> getAssetTwoRates() {
        return assetTwoRates;
    }

    public void setAssetTwoRates(List<ExchangeRate> assetTwoRates) {
        this.assetTwoRates = assetTwoRates;
    }
}
