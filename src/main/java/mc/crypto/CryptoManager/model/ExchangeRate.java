package mc.crypto.CryptoManager.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "exchange_rates")
public class ExchangeRate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "asset_one_id")
    private CurrencyType assetOne;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "asset_two_id")
    private CurrencyType assetTwo;

    private Double rate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CurrencyType getAssetOne() {
        return assetOne;
    }

    public void setAssetOne(CurrencyType assetOne) {
        this.assetOne = assetOne;
    }

    public CurrencyType getAssetTwo() {
        return assetTwo;
    }

    public void setAssetTwo(CurrencyType assetTwo) {
        this.assetTwo = assetTwo;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
