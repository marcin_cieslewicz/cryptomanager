package mc.crypto.CryptoManager.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "wallets")
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String address;

    private Double balance;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "currency_type_id")
    private CurrencyType currencyType;

    @OneToMany (mappedBy = "senderWallet")
    private List<Transaction> outflowTransactions;

    @OneToMany (mappedBy = "receiverWallet")
    private List<Transaction> inflowTransactions;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public List<Transaction> getOutflowTransactions() {
        return outflowTransactions;
    }

    public void setOutflowTransactions(List<Transaction> outflowTransactions) {
        this.outflowTransactions = outflowTransactions;
    }

    public List<Transaction> getInflowTransactions() {
        return inflowTransactions;
    }

    public void setInflowTransactions(List<Transaction> inflowTransactions) {
        this.inflowTransactions = inflowTransactions;
    }
}
