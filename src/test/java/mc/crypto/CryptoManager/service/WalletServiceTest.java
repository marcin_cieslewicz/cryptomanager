package mc.crypto.CryptoManager.service;

import mc.crypto.CryptoManager.dto.CreateWalletRequest;
import mc.crypto.CryptoManager.model.CurrencyType;
import mc.crypto.CryptoManager.model.User;
import mc.crypto.CryptoManager.model.Wallet;
import mc.crypto.CryptoManager.repository.CurrencyTypeRepository;
import mc.crypto.CryptoManager.repository.ExchangeRateRepository;
import mc.crypto.CryptoManager.repository.UserRepository;
import mc.crypto.CryptoManager.repository.WalletRepository;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class WalletServiceTest {
    @Autowired
    private WalletService walletService;

    @MockBean
    private CurrencyTypeRepository currencyTypeRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private WalletRepository walletRepository;

    @MockBean
    private ExchangeRateRepository exchangeRateRepository;

    @Captor
    private ArgumentCaptor<Wallet> walletCaptor;

    @Test
    void createWallet_noExceptionThrown_walletSuccessfullyCreated(){
        CurrencyType currencyType = new CurrencyType();
        currencyType.setId(1);
        currencyType.setCoinName("Avalanche");
        currencyType.setTicker("AVAX");

        doReturn(Optional.of(currencyType))
                .when(currencyTypeRepository)
                .findById(1);

        User user = new User();
        user.setId(1);
        user.setEmail("npc1@gmail.com");
        user.setUserName("npc1");
        user.setPassword("1cpn");

        doReturn(Optional.of(user))
                .when(userRepository)
                .findById(1);

        CreateWalletRequest createWalletRequest = new CreateWalletRequest();
        createWalletRequest.setUserId(1);
        createWalletRequest.setCurrencyTypeId(1);

        walletService.createWallet(createWalletRequest);

        Mockito.verify(walletRepository).save(walletCaptor.capture());

        Wallet wallet = walletCaptor.getValue();
        Assert.assertFalse(wallet.getAddress().isBlank());
        Assert.assertEquals(0.0, (double) wallet.getBalance(), 0);
        Assert.assertEquals(currencyType, wallet.getCurrencyType());
        Assert.assertEquals(user, wallet.getUser());
    }

    @Test
    void createWallet_exceptionCurrencyTypeNotFound_walletNotCreated(){
        doReturn(Optional.empty())
                .when(currencyTypeRepository)
                .findById(1);

        User user = new User();
        user.setId(1);
        user.setEmail("npc1@gmail.com");
        user.setUserName("npc1");
        user.setPassword("1cpn");

        doReturn(Optional.of(user))
                .when(userRepository)
                .findById(1);

        CreateWalletRequest createWalletRequest = new CreateWalletRequest();
        createWalletRequest.setUserId(1);
        createWalletRequest.setCurrencyTypeId(1);

        try {
            walletService.createWallet(createWalletRequest);
            junit.framework.TestCase.fail("An exception should be thrown");
        } catch (IllegalArgumentException exception) {
            Assert.assertEquals("Currency Type Not Found", exception.getMessage());
        }
    }

}
