package mc.crypto.CryptoManager.service;

import mc.crypto.CryptoManager.dto.GetAddressHistoryResponse;
import mc.crypto.CryptoManager.model.CurrencyType;
import mc.crypto.CryptoManager.model.Transaction;
import mc.crypto.CryptoManager.model.Wallet;
import mc.crypto.CryptoManager.repository.TransactionRepository;
import mc.crypto.CryptoManager.repository.UserRepository;
import mc.crypto.CryptoManager.repository.WalletRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class TransactionServiceTest {
    @Autowired
    private TransactionService transactionService;

    @MockBean
    private TransactionRepository transactionRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private WalletRepository walletRepository;

    @Test
    void getAddressHistory_noExceptionThrown_addressHistorySuccessfullyRetrieved() {
        CurrencyType currencyType = new CurrencyType();
        currencyType.setId(4);
        currencyType.setCoinName("Chainlink");
        currencyType.setTicker("LINK");

        Wallet requestedWallet = new Wallet();
        Wallet transactionalWallet = new Wallet();
        requestedWallet.setId(1);
        transactionalWallet.setId(4);
        requestedWallet.setAddress("720263d5-4996-430c-9098-c5b106c2e0e6");
        transactionalWallet.setAddress("5f16fe9b-d195-4623-80e7-2c6a6a0469f9");
        requestedWallet.setBalance(4.22);
        transactionalWallet.setBalance(2.021);
        requestedWallet.setCurrencyType(currencyType);
        transactionalWallet.setCurrencyType(currencyType);

        doReturn(Optional.of(requestedWallet))
                .when(walletRepository)
                .findByAddress(requestedWallet.getAddress());

        Transaction transactionOne = new Transaction();
        transactionOne.setId(1);
        transactionOne.setSenderWallet(transactionalWallet);
        transactionOne.setReceiverWallet(requestedWallet);
        transactionOne.setValue(0.1488);
        Calendar ddd = Calendar.getInstance();
        ddd.set(Calendar.YEAR, 2022);
        ddd.set(Calendar.MONTH, 2);
        ddd.set(Calendar.DAY_OF_MONTH, 5);
        ddd.set(Calendar.HOUR, 5);
        ddd.set(Calendar.MINUTE, 5);
        ddd.set(Calendar.SECOND, 5);
        ddd.set(Calendar.MILLISECOND, 5);
        transactionOne.setDate(ddd.getTime());

        requestedWallet.setInflowTransactions(new ArrayList<>());
        requestedWallet.getInflowTransactions().add(transactionOne);
        transactionalWallet.setOutflowTransactions(new ArrayList<>());
        transactionalWallet.getOutflowTransactions().add(transactionOne);

        Transaction transactionTwo = new Transaction();
        transactionTwo.setId(2);
        transactionTwo.setSenderWallet(requestedWallet);
        transactionTwo.setReceiverWallet(transactionalWallet);
        transactionTwo.setValue(1.01488);
        Calendar eee = Calendar.getInstance();
        eee.set(Calendar.YEAR, 2022);
        eee.set(Calendar.MONTH, 2);
        eee.set(Calendar.DAY_OF_MONTH, 5);
        eee.set(Calendar.HOUR, 15);
        eee.set(Calendar.MINUTE, 25);
        eee.set(Calendar.SECOND, 25);
        eee.set(Calendar.MILLISECOND, 25);
        transactionTwo.setDate(eee.getTime());

        requestedWallet.setOutflowTransactions(new ArrayList<>());
        requestedWallet.getOutflowTransactions().add(transactionTwo);
        transactionalWallet.setInflowTransactions(new ArrayList<>());
        transactionalWallet.getInflowTransactions().add(transactionTwo);

        List<GetAddressHistoryResponse> resultTransaction = transactionService.getAddressHistory(requestedWallet.getAddress());

        Assertions.assertNotNull(resultTransaction);
        Assertions.assertEquals(2, resultTransaction.size());
        Assertions.assertEquals(ddd.getTime(), resultTransaction.get(0).getTransactionDate());
        Assertions.assertEquals(eee.getTime(), resultTransaction.get(1).getTransactionDate());
        Assertions.assertEquals("LINK", resultTransaction.get(0).getTicker());
        Assertions.assertEquals(0.1488, resultTransaction.get(0).getTransactionValue());
        Assertions.assertEquals(1.01488, resultTransaction.get(1).getTransactionValue());
        Assertions.assertEquals("5f16fe9b-d195-4623-80e7-2c6a6a0469f9", resultTransaction.get(0).getSenderAddress());
        Assertions.assertEquals("720263d5-4996-430c-9098-c5b106c2e0e6", resultTransaction.get(1).getSenderAddress());
        Assertions.assertEquals("720263d5-4996-430c-9098-c5b106c2e0e6", resultTransaction.get(0).getReceiverAddress());
        Assertions.assertEquals("5f16fe9b-d195-4623-80e7-2c6a6a0469f9", resultTransaction.get(1).getReceiverAddress());
    }

    @Test
    void getAddressHistory_exceptionThrownAddressNotFound_addressHistoryFailedToRetrieve() {
        doReturn(Optional.empty())
                .when(walletRepository)
                .findByAddress("720263d5-4996-430c-9098-c5b106c2e0e6");

        try {
            transactionService.getAddressHistory("720263d5-4996-430c-9098-c5b106c2e0e6");
            junit.framework.TestCase.fail("An exception should be thrown");
        } catch (IllegalArgumentException exception) {
            Assert.assertEquals("Address Not Found", exception.getMessage());
        }
    }
}
